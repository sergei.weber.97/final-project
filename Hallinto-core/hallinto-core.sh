#!/bin/bash

create() {
	docker-compose up -d
	logs
}

start() {
	docker-compose start
}

restart() {
	docker-compose restart hallinto-core
}

stop() {
	docker-compose stop
}

destroy() {
	docker-compose down
}

logs() {
	docker-compose logs -f
}

ex() {
	docker-compose exec hallinto-core bash
}

usage() {
    echo >&2 \
	"usage: $0  [create | start | restart | stop | destroy | exec | logs]"
}

if [ $# -eq 0 ]
then
    usage
    exit 1
fi
while [ $# -gt 0 ]
do
    case "$1" in
	create) create;;
    start)  start;;
	restart)  restart;;
	stop) stop;;
	destroy) destroy;;
	ex) ex;;
	logs) logs;;
	--)	shift; break;;
	*)
	    usage
	    exit 1;;
    esac
    shift
done