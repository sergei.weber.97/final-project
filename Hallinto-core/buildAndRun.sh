#!/bin/sh
./hallinto-core.sh destroy >> /dev/null 2>&1
set -ex
mvn clean package
docker build -t de.hallinto.core/hallinto-core .
./hallinto-core.sh create