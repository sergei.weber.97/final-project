package de.hallinto.core;


import org.junit.Assert;
import org.junit.Test;

public class ExampleTest {

    @Test
    public void testAdd() {
        Example example = new Example();
        int expected = 3, actual = example.add(1, 2);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddInvalid() {
        Example example = new Example();
        int expected = 3, actual = example.add(1, -1);

        Assert.assertNotEquals(expected, actual);
    }
}
