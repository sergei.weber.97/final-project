package de.hallinto.core.rest;

import de.hallinto.core.model.dao.BelegungDAO;
import de.hallinto.core.model.entity.Belegung;
import de.hallinto.core.rest.api.BelegungApiService;
import org.apache.commons.lang3.time.DateUtils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.*;

/**
 *
 */
@RequestScoped
@Path("/srvBelegung")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public class BelegungApiServiceImpl implements BelegungApiService {

    @Inject
    BelegungDAO belegungDAO;
    private long lastIstBis;

    /**
     * @param belegung        Belegung die gespeichert werden soll
     * @param securityContext
     * @return
     */
    @Override
    public Response addBelegung(Belegung belegung, SecurityContext securityContext) {
        // generate token for new entities
        if (belegung.getId() == null || belegung.getId().equals("") || belegung.getToken() == null || belegung.getToken().equals("")) {
            String sToken = "";
            belegung.setToken(UUID.randomUUID().toString());
        }

        if(belegung.getSollUhrzeitBis().compareTo(belegung.getSollUhrzeitVon()) <= 0)
            return Response.serverError().build();

        belegungDAO.persist(belegung);
        return Response.ok().entity(belegung).build();
    }

    /**
     * @param id              ID der Belegung
     * @param securityContext
     * @return
     */
    @Override
    public Response deleteBelegung(String id, SecurityContext securityContext) {
        belegungDAO.removeById(id);
        return Response.ok().build();
    }

    /**
     * @param limit           Anzahl auf die die Ergebnisse limitiert werden sollen
     * @param securityContext
     * @return
     */
    @Override
    public Response findBelegung(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(belegungDAO.findAll(limit)).build();
    }

    /**
     * @param id              ID der Belegung
     * @param securityContext
     * @return
     */
    @Override
    public Response findBelegungById(String id, SecurityContext securityContext) {
        Belegung belegung = belegungDAO.findById(id);
        belegung = calcTimeForSingle(belegung);

        return Response.ok().entity(belegung).build();
    }

    private Belegung calculateIstZeit(Belegung belegung) {
        return calculateIstZeit(belegung, true);
    }

    private Belegung calculateIstZeit(Belegung belegung, boolean resetLastIstBis) {
        return _calculateIstZeit(belegung, resetLastIstBis);
    }

    private Belegung _calculateIstZeit(Belegung belegung, boolean resetLastIstBis) {
        Date now = new Date();
        if (resetLastIstBis)
            lastIstBis = now.getTime();

        if (DateUtils.isSameDay(belegung.getDatum(), now)) {
            long timeSollStart = belegung.getSollUhrzeitVon().getTime();
            long localDelta = lastIstBis - timeSollStart;
            long timeSollEnd = belegung.getSollUhrzeitBis().getTime();

            if (belegung.getCurrentAktive() == true) {
                System.out.println("hier ist er");
                // dieser hier ist einer, der aktuell sein könnte, darum in die prozess-liste pushen
                // calcList.push(belegung);
                // aktuelle verzögerung zum soll start ermitteln
                long timeIstStart = belegung.getIstUhrzeitVon().getTime();
                long timeDelta = timeIstStart - timeSollStart;
                long duration = (timeSollEnd - timeSollStart);

                long timeIstBis = timeSollEnd + timeDelta;

                if(timeIstStart + duration < timeSollStart + localDelta) {
                    timeIstBis += Math.abs(timeDelta);

                    timeIstBis += timeSollStart + localDelta - timeIstBis;
                }

                Date newIstBis = new Date(timeIstBis);

                belegung.setIstUhrzeitBis(newIstBis);

                lastIstBis = newIstBis.getTime();
            }  else if(belegung.getFinished() == false) {
                if(localDelta >= 0) {
                    // ... dieser hier sollte schon laufen, ist aber noch nicht angenommen ...
                    Date newIstVon = new Date(timeSollStart + localDelta);
                    Date newIstBis = new Date(timeSollEnd + localDelta);

                    belegung.setIstUhrzeitVon(newIstVon);
                    belegung.setIstUhrzeitBis(newIstBis);

                    // new global-delta is current shift + duration
                    lastIstBis = newIstBis.getTime();
                } else {
                    // ... dieser hier liegt eh noch in der zukunft - alles easy

                    // TODO: hier abfragen, ob noch aktuell einer läuft => könnte delta geben
                    Date newIstVon = new Date(timeSollStart);
                    Date newIstBis = new Date(timeSollEnd);

                    belegung.setIstUhrzeitVon(newIstVon);
                    belegung.setIstUhrzeitBis(newIstBis);

                    lastIstBis = newIstBis.getTime();
                }

            }
        }

        return belegung;
    }

    @GET
    @Path("/getByRaumIdAndStartEnd")
    @Produces({"application/json"})
    public Response getByRaumIdAndStartEnd(@QueryParam("rID") String sRaumId, @QueryParam("s") long start, @QueryParam("e") long end) {
        List<Belegung> belegungList = belegungDAO.getByRaumIdAndStartEnd(sRaumId, start, end);

        // order by sollstartzeit
        belegungList.sort(new Comparator<Belegung>() {
            @Override
            public int compare(Belegung o1, Belegung o2) {
                if (o1.getCurrentAktive() == true) {
                    return -1;
                }
                if (o2.getCurrentAktive() == true) {
                    return 1;
                }
                if (o1.getSollUhrzeitVon().before(o2.getSollUhrzeitVon())) {
                    return -1;
                }
                if (o1.getSollUhrzeitVon().after(o2.getSollUhrzeitVon())) {
                    return 1;
                }
                return 0;
            }
        });


        final List<Belegung> belegungListRet = new ArrayList<>(belegungList.size());

        // dynamisch nur für den aktuellen tag - historie ist persistent - und alles was vergangen ist (beide zeiten gebucht)
        boolean bReset = true;

        for (Belegung belegung : belegungList) {
            belegungListRet.add(calculateIstZeit(belegung, bReset));
            if (bReset) {
                bReset = false;
            }
        }

        return Response.ok().entity(belegungListRet).build();
    }

    private Belegung calcTimeForSingle(Belegung belegung) {
        if(belegung.getCurrentAktive() == false && belegung.getFinished() == false) {
            // is there a current one active in same room? => add potential delta from current handle!!!
            Belegung currentActiveBelegung = belegungDAO.getByRaumIdAndCurrentAktive(belegung.getRaum().getId());
            if(currentActiveBelegung != null) {
                calculateIstZeit(currentActiveBelegung, true);
                belegung = calculateIstZeit(belegung, false);
            } else {
                belegung = calculateIstZeit(belegung, true);
            }
        } else {
            belegung = calculateIstZeit(belegung, true);
        }
        return belegung;
    }

    @GET
    @Path("/getByToken/{token}")
    @Produces({"application/json"})
    public Response getByToken(@PathParam("token") String sToken) {
        Belegung belegung = belegungDAO.getByToken(sToken);

        belegung = calcTimeForSingle(belegung);

        return Response.ok().entity(belegung).build();
    }
}
