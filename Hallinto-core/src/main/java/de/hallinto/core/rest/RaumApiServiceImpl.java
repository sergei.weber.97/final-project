package de.hallinto.core.rest;

import de.hallinto.core.model.dao.BelegungDAO;
import de.hallinto.core.model.dao.RaumDAO;
import de.hallinto.core.model.entity.Raum;
import de.hallinto.core.rest.api.RaumApiService;

import javax.inject.Inject;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

public class RaumApiServiceImpl implements RaumApiService {

    @Inject
    RaumDAO daoRaum;

    @Inject
    BelegungDAO belegungDAO;

    @Override
    public Response addRaum(Raum raum, SecurityContext securityContext) {
        List<Raum> raumList = daoRaum.findAll();

        boolean bDontAdd = false;
        for (Raum raum1 : raumList) {
            if(raum1.getBezeichnung().equals(raum.getBezeichnung()) && raum1.getNummer() == raum.getNummer()) {
                bDontAdd = true;
                break;
            }
        }
        if(!bDontAdd)
            return Response.ok().entity(daoRaum.persist(raum)).build();

        return Response.serverError().build();
    }

    @Override
    public Response deleteRaum(String id, SecurityContext securityContext) {
        belegungDAO.getByRaumId(id).forEach(belegung -> belegungDAO.remove(belegung));

        daoRaum.remove(daoRaum.findById(id));
        return Response.ok().build();
    }

    @Override
    public Response findRaumById(String id, SecurityContext securityContext) {
        return Response.ok().entity(daoRaum.findById(id)).build();
    }

    @Override
    public Response findAllRaum(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(daoRaum.findAll(limit)).build();
    }
}
