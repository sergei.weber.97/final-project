package de.hallinto.core.rest;

import de.hallinto.core.model.dao.BelegungDAO;
import de.hallinto.core.model.dao.PatientDAO;
import de.hallinto.core.model.entity.Patient;
import de.hallinto.core.rest.api.PatientenApiService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.util.List;

@RequestScoped
@Path("/svc/patienten")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public class PatientenApiServiceImpl implements PatientenApiService {

    @Inject
    PatientDAO patientDAO;

    @Inject
    BelegungDAO belegungDAO;

    /**
     * @param patient         Patient der gespeichert werden soll
     * @param securityContext
     * @return
     */
    @Override
    public Response addPatient(Patient patient, SecurityContext securityContext) {
        patientDAO.persist(patient);
        return Response.ok().entity(patient).build();
    }

    /**
     * @param id              ID des Patienten
     * @param securityContext
     * @return
     */
    @Override
    public Response deletePatient(String id, SecurityContext securityContext) {
        // delete corresponding Belegungen
        belegungDAO.getByPatId(id).forEach(belegung -> belegungDAO.remove(belegung));

        patientDAO.removeById(id);
        return Response.ok().build();
    }

    /**
     * @param id              ID des Patienten
     * @param securityContext
     * @return
     */
    @Override
    public Response findPatientById(String id, SecurityContext securityContext) {
        return Response.ok().entity(patientDAO.findById(id)).build();
    }

    /**
     * @param limit           Anzahl auf die die Ergebnisse limitiert werden sollen
     * @param securityContext
     * @return
     */
    @Override
    public Response findPatienten(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(patientDAO.findAll(limit)).build();
    }

    @GET
    @Path("/findPatientenByFilter")
    @Produces({"application/json"})
    public Response findPatientenByFilter(@QueryParam("name") String sFilterName,
                                          @QueryParam("vorname") String sFilterNachname,
                                          @QueryParam("bday") String sFilterBday) {
        List<Patient> patientList = null;

        if ((sFilterName == null || sFilterName.equals(""))
                && (sFilterNachname == null || sFilterNachname.equals(""))
                && (sFilterBday == null || sFilterBday.equals(""))) {
            patientList = patientDAO.findAll();
        } else {
            patientList = patientDAO.findByFilter(sFilterName, sFilterNachname, sFilterBday);
        }

        return Response.ok().entity(patientList).build();
    }
}
