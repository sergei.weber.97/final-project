package de.hallinto.core.rest;

import de.hallinto.core.model.dao.MitarbeiterDAO;
import de.hallinto.core.model.entity.Mitarbeiter;
import de.hallinto.core.rest.api.MitarbeiterApiService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/mitarbeiterservice")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public class MitarbeiterApiServiceImpl implements MitarbeiterApiService {

    @Inject
    MitarbeiterDAO mitarbeiterDAO;

    /**
     * @param mitarbeiter     Mitarbeiter der gespeichert werden soll
     * @param securityContext
     * @return
     */
    @Override
    public Response addMitarbeiter(Mitarbeiter mitarbeiter, SecurityContext securityContext) {
        // check for duplicate username
        try {
            Mitarbeiter mitarbeiter1 = mitarbeiterDAO.findByUsername(mitarbeiter.getUsername());
            if(mitarbeiter1.getId().equals(mitarbeiter.getId()))
                return Response.ok().entity(mitarbeiterDAO.persist(mitarbeiter)).build();
        } catch (Exception e) {
            return Response.ok().entity(mitarbeiterDAO.persist(mitarbeiter)).build();
        }

        return Response.serverError().build();
    }

    /**
     * @param id              ID des Mitarbeiters
     * @param securityContext
     * @return
     */
    @Override
    public Response deleteMitarbeiter(String id, SecurityContext securityContext) {
        if(mitarbeiterDAO.findAll().size() > 1) {
            mitarbeiterDAO.removeById(id);
            return Response.ok().build();
        }
        return Response.serverError().build();
    }

    /**
     * @param limit           Anzahl auf die die Ergebnisse limitiert werden sollen
     * @param securityContext
     * @return
     */
    @Override
    public Response findMitarbeiter(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(mitarbeiterDAO.findAll(limit)).build();
    }

    /**
     * @param id              ID des Mitarbeiters
     * @param securityContext
     * @return
     */
    @Override
    public Response findMitarbeiterById(String id, SecurityContext securityContext) {
        return Response.ok().entity(mitarbeiterDAO.findById(id)).build();
    }

    /**
     * @param username Benutzername des Mitarbeiters
     * @param password Passwort des Mitarbeiters
     * @return
     */

    @Override
    @GET
    @Path("/login/{username}")
    @Produces({"application/json"})
    public Boolean login(@PathParam("username") String username, @QueryParam("password") String password) {
        try {
            Mitarbeiter mitarbeiter = mitarbeiterDAO.findByUsername(username);

            if (mitarbeiter.getPassword().equals(password))
                return true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
