package de.hallinto.core.rest;

import de.hallinto.core.model.dao.PersonDAO;
import de.hallinto.core.model.entity.Person;
import de.hallinto.core.rest.api.PersonApiService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@RequestScoped
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")
public class PersonApiServiceImpl implements PersonApiService {

    @Inject
    PersonDAO personDAO;

    /**
     * @param person          Person der gespeichert werden soll
     * @param securityContext
     * @return
     */
    @Override
    public Response addPerson(Person person, SecurityContext securityContext) {
        personDAO.persist(person);
        return Response.ok().entity(person).build();
    }

    /**
     * @param id              ID der Person
     * @param securityContext
     * @return
     */
    @Override
    public Response deletePerson(String id, SecurityContext securityContext) {
        personDAO.removeById(id);
        return Response.ok().build();
    }

    /**
     * @param limit           Anzahl auf die die Ergebnisse limitiert werden sollen
     * @param securityContext
     * @return
     */
    @Override
    public Response findPerson(Integer limit, SecurityContext securityContext) {
        return Response.ok().entity(personDAO.findAll(limit)).build();
    }

    /**
     * @param id              ID der Person
     * @param securityContext
     * @return
     */
    @Override
    public Response findPersonById(String id, SecurityContext securityContext) {
        return Response.ok().entity(personDAO.findById(id)).build();
    }
}
