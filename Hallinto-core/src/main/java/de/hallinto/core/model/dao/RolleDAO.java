package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Rolle;

/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Rolle.
 *
 * @see Rolle
 * @see de.hallinto.core.model.dao.GenericDAO
 */
public class RolleDAO extends GenericDAO<Rolle> {

}
