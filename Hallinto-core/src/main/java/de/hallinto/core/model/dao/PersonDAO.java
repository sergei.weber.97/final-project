package de.hallinto.core.model.dao;

import de.hallinto.core.model.entity.Person;

/**
 * Data Access Object (DAO) zur vereinfachten Handhabung der Entity Person.
 *
 * @see Person
 * @see de.hallinto.core.model.dao.GenericDAO
 */
public class PersonDAO extends GenericDAO<Person> {
}
