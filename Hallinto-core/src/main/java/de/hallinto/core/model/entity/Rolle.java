package de.hallinto.core.model.entity;

import javax.persistence.Entity;

@Entity
public class Rolle extends HEntity {
    private String bezeichnung;

    /**
     * @return Bezeichnung der Rolle
     */
    public String getBezeichnung() {
        return bezeichnung;
    }

    /**
     * @param bezeichnung
     */
    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }
}
