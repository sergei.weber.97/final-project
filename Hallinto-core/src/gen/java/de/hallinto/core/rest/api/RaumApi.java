package de.hallinto.core.rest.api;

import de.hallinto.core.model.entity.Raum;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/raum")
@RequestScoped

@Consumes({"application/json"})
@Produces({"application/json"})
public class RaumApi {

    @Context
    SecurityContext securityContext;
    @Inject
    RaumApiService delegate;

    @POST
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response addRaum(Raum raum) {
        return delegate.addRaum(raum, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response deleteRaum(@PathParam("id") String id) {
        return delegate.deleteRaum(id, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response findRaumById(@PathParam("id") String id) {
        return delegate.findRaumById(id, securityContext);
    }

    @GET
    @Consumes({"application/json"})
    @Produces({"application/json"})
    public Response findAllRaum(@QueryParam("limit") Integer limit) {
        return delegate.findAllRaum(limit, securityContext);
    }
}
