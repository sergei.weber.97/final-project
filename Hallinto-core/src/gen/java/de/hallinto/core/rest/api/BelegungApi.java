package de.hallinto.core.rest.api;

import de.hallinto.core.ErrorResponse;
import de.hallinto.core.model.entity.Belegung;
import io.swagger.annotations.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/belegung")
@RequestScoped

@Api(description = "the belegung API")
@Consumes({"application/json"})
@Produces({"application/json"})
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaJAXRSCXFCDIServerCodegen", date = "2018-04-26T21:20:51.095+02:00")

public class BelegungApi {

    @Context
    SecurityContext securityContext;

    @Inject
    BelegungApiService delegate;


    @POST

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Erstellt eine neue Belegung", response = Belegung.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Erstellte Belegung", response = Belegung.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response addBelegung(@ApiParam(value = "Belegung die erstellt werden soll", required = true) Belegung belegung) {
        return delegate.addBelegung(belegung, securityContext);
    }

    @DELETE
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Löscht eine einzelne Belegung basierend auf ihrer ID", response = Void.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Erfolgreiche Löschung", response = Void.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response deleteBelegung(@ApiParam(value = "ID der Belegung", required = true) @PathParam("id") String id) {
        return delegate.deleteBelegung(id, securityContext);
    }

    @GET

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt alle Belegungen zurück", response = Belegung.class, responseContainer = "List", tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Ausgabe aller oder limitierter Anzahl der Belegungen als Array", response = Belegung.class, responseContainer = "List"),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findBelegung(@ApiParam(value = "Maximale Anzahl der Ergebnisse") @QueryParam("limit") Integer limit) {
        return delegate.findBelegung(limit, securityContext);
    }

    @GET
    @Path("/{id}")
    @Consumes({"application/json"})
    @Produces({"application/json"})
    @ApiOperation(value = "", notes = "Gibt eine einzelne Belegung basierend auf seiner ID zurück", response = Belegung.class, tags = {})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Belegung aus Datenbank welche gegebener ID entspricht", response = Belegung.class),
            @ApiResponse(code = 200, message = "Unerwarteter Fehler", response = ErrorResponse.class)})
    public Response findBelegungById(@ApiParam(value = "ID einer Belegung nach der gesucht werden soll", required = true) @PathParam("id") String id) {
        return delegate.findBelegungById(id, securityContext);
    }
}
