# hallinto-ui-ionic

## Abhänigkeiten
* NodeJS
* Ionic
* laufendes Hallinto-core Backend

## Kompilieren & Ausführen (Installieren)
Zunächst muss Ionic via NPM installiert sein.

```
./npm install -g ionic
```

Anschließend muss man in der Datei

```
./src/providers/registry/registry.ts
```

die entsprechend richtige Backend-API-URL (apiUrl) hinterlegen, damit das Frontend mit dem (richtigen) Backend kommunizieren kann.

Nun kann das richtig konfigurierte Frontend kompiliert und gestartet werden via:

```
./ionic serve
```

## Zugriff

Da das Ionic-Framework auf Web-Basis agiert, ist das Frontend (bei richtiger Firewallkonfiguration) von jedem beliebigen Host aus über die eigene IP/Hostname an Port 8100 zu erreichen.

## Ansichten

### Mitarbeiter-Ansicht
Die Mitarbeiter-Ansicht ist die Default-View für die Applikation.

```
http://<hostname>:8100/
```

### Patienten-Ansicht
Die Patienten-Ansicht (>> Eingabe des Belegungs-Token / Übersicht der eigenen Belegung) ist nur "manuell" zu erreichen, unter:

```
http://<hostname>:8100/#/belegung-einsehen
```

## Beispiel Instanz
#### (nur innerhalb OTH-LAN/VPN)
Eine Beispiel-Instanz kann mittels der uns zugewiesenen VM (im-vm-035) erprobt werden.

```
http://im-vm-035:8100/
```
