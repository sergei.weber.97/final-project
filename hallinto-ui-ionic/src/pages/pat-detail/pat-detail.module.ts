import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatDetailPage } from './pat-detail';

@NgModule({
  declarations: [
    PatDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PatDetailPage),
  ],
})
export class PatDetailPageModule {}
