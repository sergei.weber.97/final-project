import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RaumPlanPage } from './raum-plan';

@NgModule({
  declarations: [
    RaumPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(RaumPlanPage),
  ],
})
export class RaumPlanPageModule {}
