import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {RaumProvider} from "../../providers/raum/raum";

import $ from 'jquery';
import 'fullcalendar';

import 'fullcalendar/dist/locale/de'
import {BelegungProvider} from "../../providers/belegung/belegung";
import {PatientProvider} from "../../providers/patient/patient";

/**
 * Generated class for the RaumPlanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-raum-plan',
  templateUrl: 'raum-plan.html',
})
export class RaumPlanPage {

  private selectedRaumId;

  private calendar;

  private refreshInterval;

  public eRaum: any = {
    bezeichnung: "",
    nummer: null
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, private raum: RaumProvider, public alert: AlertController, public belegung: BelegungProvider, public patProv: PatientProvider) {
    this.selectedRaumId = this.navParams.get("room");

    window['that'] = this;

    this.refreshInterval = null;
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter RaumPlanPage');


    this.calendar.fullCalendar('refetchEvents');

    this.refreshInterval = setInterval(() => {
      console.log("refreshing");
      this.calendar.fullCalendar('refetchEvents');
    }, 60 * 1000);

  }

  ionViewWillLeave() {
    if(this.refreshInterval != null)
      clearInterval(this.refreshInterval);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RaumPlanPage');

    this.raum.getById(this.selectedRaumId).then((entityRaum) => {
      this.eRaum = entityRaum;
    });

    this.calendar = $('#calendar');

    this.calendar.fullCalendar({
      weekends: false, // will hide Saturdays and Sundays
      defaultView: "agendaWeek",
      allDaySlot: false,
      locale: "de",
      slotDuration: "00:15:00",
      slotLabelFormat: "H:mm",
      defaultTimedEventDuration: '00:30:00',
      forceEventDuration: true,
      selectable: true,
      selectHelper: true,
      timezone: 'local',
      events: this.getFCEvents,
      select: this.FCSlotSelected,
      eventClick: this.FCEventClick,
      displayEventTime: true,
      displayEventEnd: true,
      nowIndicator: true,
      eventDrop: this.FCEventDrop,
      minTime: "07:00:00",
      maxTime: "20:00:00"
    });
/*
    this.refreshInterval = setInterval(() => {
      console.log("refreshing");
      this.calendar.fullCalendar('refetchEvents');
    }, 60 * 1000); */

  }

  private FCEventDrop(event, delta, revertFunc) {

  }

  private FCEventClick(event, jsEvent, view) {
    // out of angular-scope ... too bad
    var that = window['that'];

    that.navCtrl.push('BelegungDetailPage', {belegungId: event.id});

  }

  private FCSlotSelected(start, end, jsEvent, view, resource) {

    // out of angular-scope ... too bad
    var that = window['that'];

    let alert = that.alert.create({
      title: start.format("HH:mm") + ' - ' + end.format("HH:mm") + ' (' + start.format("DD.MM.YYYY") + ')',
      subTitle: 'Soll im gewählten Zeitraum ein Termin vergeben werden?',
      buttons: [
        {
          text: 'Ja',
          handler: () => {
            that.navCtrl.push('CreateBelegungPage', {room: that.eRaum, start: start, end: end});
          }
        },
        {
          text: 'Nein',
          handler: () => {
            this.calendar.unselect();
          }
        }
      ]
    });
    alert.present();
  }

  private getFCEvents(start, end, timezone, callback) {
    // ... get events

    var that = window['that'];

    let events = [];

    that.belegung.getByRaumIdAndStartEnd(that.selectedRaumId, start.unix(), end.unix()).then((lstAll) => {

      var that_ = that;
      lstAll.forEach((beleg: any) => {
        beleg.pat = that_.patProv.addOwnToString(beleg.pat);

        // push ist-termine
        events.push({
          id: beleg.id+"-ist",
          title: beleg.pat,
          start: beleg.istUhrzeitVon,
          end: beleg.istUhrzeitBis,
          color: beleg.currentAktive ? "green" : beleg.finished ? "purple" : "yellow",
          textColor: "black"
        });

        // push soll-termine
        events.push({
          id: beleg.id,
          title: beleg.pat,
          start: beleg.sollUhrzeitVon,
          end: beleg.sollUhrzeitBis,
          textColor: beleg.currentAktive ? "green" : beleg.finished ? "purple" : "white",
          editable: false
        });


      });

      callback(events);

    });

  }

}
