import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PatientProvider} from "../../providers/patient/patient";

/**
 * Generated class for the PatSucheResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pat-suche-result',
  templateUrl: 'pat-suche-result.html',
})
export class PatSucheResultPage {

  currentFilter = "";
  queriedPatienten = null;

  modalResolv = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, public patient: PatientProvider) {
    let filter = navParams.get("query");

    if(filter != null)
      if(filter.name == "" && filter.vorname == "" && filter.bday == "")
        this.currentFilter = "<alle>";
      else {
        if(filter.name != "") {
          this.currentFilter += filter.name;
        }

        if(filter.vorname != "") {
          if(filter.name != "") {
            this.currentFilter += ", ";
          }
          this.currentFilter += filter.vorname;
        }

        if(filter.bday != "") {
          if(filter.name != "" || filter.vorname != "") {
            this.currentFilter += ", ";
          }
          this.currentFilter += "*"+this.dateToString(filter.bday);
        }
      }

    this.queriedPatienten = navParams.get("response");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatSucheResultPage');

    if(this.navParams.get("modal"))
      this.modalResolv = this.navParams.get("modal");
  }

  dateToString(input) {
    let date = new Date(input);
    let day = date.getDate();
    let month = date.getMonth() + 1;

      return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + date.getFullYear();
  }

  openDetail(pat) {
    console.log(pat.id);
    if(this.modalResolv) {
      // return selected pat to parent
      this.modalResolv(pat);
      this.navCtrl.popTo(this.navCtrl.getByIndex(this.navCtrl.length()-3));
    } else {
      this.patient.getById(pat.id).then(pat => {
        this.navCtrl.push('PatDetailPage', {patData: pat});
      });
    }
  }


  addPatient() {
    this.navCtrl.push('PatCreatePage', {});
  }

}
