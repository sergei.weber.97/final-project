import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PatientProvider} from "../../providers/patient/patient";

/**
 * Generated class for the PatCreatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pat-create',
  templateUrl: 'pat-create.html',
})
export class PatCreatePage {

  patForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public patient: PatientProvider, public toastCtrl: ToastController) {

    this.patForm = this.formBuilder.group({
      vorname: ["", Validators.required],
      nachname: ["", Validators.required],
      geschlecht: ["", Validators.required],
      geburtsdatum: ["", Validators.required],
      kostentraeger: ["", Validators.required],
      krankenkasse: ["", Validators.required],
      versicherungsnummer: ["", Validators.required],
      versicherungsstatus: ["", Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatCreatePage');
  }

  createPat() {
    this.patient.save(this.patForm.value).then((data: any) => {
      console.dir(data);
      if(data != null && data.id != null) {
        let toast = this.toastCtrl.create({
          message: 'Patient erfolgreich angelegt!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.popToRoot();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Patient anlegen gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    });
  }

}
