import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MitarbeiterDetailPage } from './mitarbeiter-detail';

@NgModule({
  declarations: [
    MitarbeiterDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MitarbeiterDetailPage),
  ],
})
export class MitarbeiterDetailPageModule {}
