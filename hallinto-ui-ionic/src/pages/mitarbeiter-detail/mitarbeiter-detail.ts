import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";

/**
 * Generated class for the MitarbeiterDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mitarbeiter-detail',
  templateUrl: 'mitarbeiter-detail.html',
})
export class MitarbeiterDetailPage {

  mitData: any = {
    id: "",
    vorname: "",
    nachname: "",
    username: "",
  };

  mitForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public mitarbeiter: MitarbeiterProvider, public toastCtrl: ToastController, public alertCtrl: AlertController) {
    this.mitData = navParams.get("mitData");

    this.mitForm = this.formBuilder.group({
      id: [this.mitData.id],
      vorname: [this.mitData.vorname, Validators.required],
      nachname: [this.mitData.nachname, Validators.required],
      username: [this.mitData.username, Validators.required],
      password: [this.mitData.password, Validators.required],
      rollen: [this.mitData.rollen != null && this.mitData.rollen[0] != null && this.mitData.rollen[0].id == "5b0542d102743900367cd028" ? true : false]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MitarbeiterDetailPage');
  }

  saveMit() {
    let dataToSave = this.mitForm.value;
    if(dataToSave.rollen == true) {
      dataToSave.rollen = [{
        id: "5b0542d102743900367cd028"
      }];
    } else {
      dataToSave.rollen = [];
    }

    this.mitarbeiter.save(dataToSave).then((data: any) => {
      console.dir(data);
      if(data != null && data.id != null) {
        this.mitData = data;
        let toast = this.toastCtrl.create({
          message: 'Speichern erfolgreich!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.popToRoot();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Speichern gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Speichern gescheitert!',
        duration: 3000
      });
      toast.present();
    });
  }

  deleteMit() {
    let alert = this.alertCtrl.create({
      title: 'Datensatz löschen',
      subTitle: 'Mitarbeiter "' + this.mitData.nachname + ', ' + this.mitData.vorname + ' (' + this.mitData.username + ')" wirklich löschen?',
      buttons: [
        {
          text: 'Ja, löschen',
          handler: () => {
            this.mitarbeiter.delete(this.mitData.id).then(data => {
              let toast = this.toastCtrl.create({
                message: 'Datensatz gelöscht.',
                duration: 1500
              });
              toast.present().then(() => {
                this.navCtrl.popToRoot();
              });
            }).catch((err) => {
              let toast = this.toastCtrl.create({
                message: 'Löschen gescheitert!',
                duration: 3000
              });
              toast.present();
            });
          }
        },
        {
          text: 'Nein',
          handler: () => {
            console.log('Do not delete');
          }
        }
      ]
    });
    alert.present();
  }

}
