import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BelegungProvider} from "../../providers/belegung/belegung";
import * as moment from "moment";

/**
 * Generated class for the BelegungEinsehenDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-belegung-einsehen-detail',
  templateUrl: 'belegung-einsehen-detail.html',
})
export class BelegungEinsehenDetailPage {

  public beleg;

  public belegungForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public belegung: BelegungProvider, private formBuilder: FormBuilder) {
    this.belegungForm = this.formBuilder.group({
      datum: ["", Validators.required],
      sollUhrzeitVon: ["", Validators.required],
      sollUhrzeitBis: ["", Validators.required],
      istUhrzeitVon: ["", Validators.required],
      istUhrzeitBis: ["", Validators.required],
      pat: ["", Validators.required],
      token: ["", Validators.required]
    });
  }

  private dateToString(input) {
    let date = new Date(input);
    let day = date.getDate();
    let month = date.getMonth() + 1;

    return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + date.getFullYear();
  }

  private addOwnToString(patEntityObject) {
    // for readability, overload toString method ...
    var that = this;
    patEntityObject.toString = function() {
      return this.nachname + ", " + this.vorname + ", *" + that.dateToString(this.geburtsdatum);
    };

    return patEntityObject;
  }

  setIonicDateTime(value: string) {
    if (value) {
      let date = moment(value);
      //let ionicDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());

      console.log(date.toISOString(true));

      return date;
    }
    return null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BelegungEinsehenDetailPage');

    this.beleg = this.navParams.get("belegung");

    //let token: String = this.navParams.get("token");

    // this.belegung.getByToken(token).then((data) => {
    //  this.beleg = data;

      this.belegungForm.setValue({
        datum: this.setIonicDateTime(this.beleg.datum).toISOString(true),
        sollUhrzeitVon: this.setIonicDateTime(this.beleg.sollUhrzeitVon).toISOString(true),
        sollUhrzeitBis: this.setIonicDateTime(this.beleg.sollUhrzeitBis).toISOString(true),
        istUhrzeitVon: this.setIonicDateTime(this.beleg.istUhrzeitVon).toISOString(true),
        istUhrzeitBis: this.setIonicDateTime(this.beleg.istUhrzeitBis).toISOString(true),
        pat: this.addOwnToString(this.beleg.pat),
        token: this.beleg.token
      });
    //});
  }

}
