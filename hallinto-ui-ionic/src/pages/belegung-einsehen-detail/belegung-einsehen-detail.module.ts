import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BelegungEinsehenDetailPage } from './belegung-einsehen-detail';

@NgModule({
  declarations: [
    BelegungEinsehenDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BelegungEinsehenDetailPage),
  ],
})
export class BelegungEinsehenDetailPageModule {}
