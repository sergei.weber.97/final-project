import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {PatientProvider} from "../../providers/patient/patient";
import {PatSucheResultPage} from "../pat-suche-result/pat-suche-result";

/**
 * Generated class for the PatSuchePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pat-suche',
  templateUrl: 'pat-suche.html',
})
export class PatSuchePage {

  searchForm: FormGroup;

  modalResolv = null;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, public patient: PatientProvider) {
    this.searchForm = this.formBuilder.group({
      name: [''],
      vorname: [''],
      bday: ['']
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatSuchePage');

    if(this.navParams.get("modal"))
      this.modalResolv = this.navParams.get("modal");

  }

  onSearch() {
    this.patient.suche(this.searchForm.value).then(lstPatienten => {
      console.log(lstPatienten);
      this.navCtrl.push('PatSucheResultPage', {query: this.searchForm.value, response: lstPatienten, modal: this.modalResolv});
    });
  }

  clearBday() {
    this.searchForm.controls['bday'].setValue("");
  }

}
