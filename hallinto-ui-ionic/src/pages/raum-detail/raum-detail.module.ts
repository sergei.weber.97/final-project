import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RaumDetailPage } from './raum-detail';

@NgModule({
  declarations: [
    RaumDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RaumDetailPage),
  ],
})
export class RaumDetailPageModule {}
