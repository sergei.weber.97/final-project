import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';
import {BelegungProvider} from "../../providers/belegung/belegung";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

import * as moment from 'moment';
import 'moment/locale/de';

/**
 * Generated class for the BelegungDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-belegung-detail',
  templateUrl: 'belegung-detail.html',
})
export class BelegungDetailPage {

  public beleg;

  public belegungForm: FormGroup;

  private viewIstTime: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public belegung: BelegungProvider, private formBuilder: FormBuilder, public alertCtrl: AlertController, public toastCtrl: ToastController) {
    this.belegungForm = this.formBuilder.group({
      datum: ["", Validators.required],
      sollUhrzeitVon: ["", Validators.required],
      sollUhrzeitBis: ["", Validators.required],
      istUhrzeitVon: ["", Validators.required],
      istUhrzeitBis: ["", Validators.required],
      pat: ["", Validators.required],
      token: ["", Validators.required]
    });
  }

  private dateToString(input) {
    let date = new Date(input);
    let day = date.getDate();
    let month = date.getMonth() + 1;

    return (day < 10 ? "0" : "") + day + "." + (month < 10 ? "0" : "") + month + "." + date.getFullYear();
  }

  private addOwnToString(patEntityObject) {
    // for readability, overload toString method ...
    var that = this;
    patEntityObject.toString = function() {
      return this.nachname + ", " + this.vorname + ", *" + that.dateToString(this.geburtsdatum);
    };

    return patEntityObject;
  }

  setIonicDateTime(value: string) {
    if (value) {
      let date = moment(value);
      //let ionicDate = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds(), date.getUTCMilliseconds());

      console.log(date.toISOString(true));

      return date;
    }
    return null;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BelegungDetailPage');

    let belegId: String = this.navParams.get("belegungId");

    // hier prüfen, ob etwa ein ist-termin erfasst wurden, anhand der id (<id>-ist)

    if(belegId.endsWith("-ist")) {
      // splitt id und ist-soll switch aktivieren ...
      this.viewIstTime = true;
      belegId = belegId.slice(0, belegId.length - 4);
    }

    this.belegung.getById(belegId).then((data) => {
      this.beleg = data;
      console.dir(this.beleg);

      this.belegungForm.setValue({
        datum: this.setIonicDateTime(this.beleg.datum).toISOString(true),
        sollUhrzeitVon: this.setIonicDateTime(this.beleg.sollUhrzeitVon).toISOString(true),
        sollUhrzeitBis: this.setIonicDateTime(this.beleg.sollUhrzeitBis).toISOString(true),
        istUhrzeitVon: this.setIonicDateTime(this.beleg.istUhrzeitVon).toISOString(true),
        istUhrzeitBis: this.setIonicDateTime(this.beleg.istUhrzeitBis).toISOString(true),
        pat: this.addOwnToString(this.beleg.pat),
        token: this.beleg.token
      });
    });
  }

  deleteBeleg() {
    let alert = this.alertCtrl.create({
      title: 'Datensatz löschen',
      subTitle: 'Belegung wirklich löschen?',
      buttons: [
        {
          text: 'Ja, löschen',
          handler: () => {
            this.belegung.delete(this.beleg.id).then(data => {
              let toast = this.toastCtrl.create({
                message: 'Datensatz gelöscht.',
                duration: 1500
              });
              toast.present().then(() => {
                this.navCtrl.pop();
              });
            });
          }
        },
        {
          text: 'Nein',
          handler: () => {
            console.log('Do not delete');
          }
        }
      ]
    });
    alert.present();
  }

  saveBeleg(handleStart?, handleEnd?) {

    let dataToSave = this.belegungForm.value;


    this.beleg.datum = dataToSave.datum;

      this.beleg.sollUhrzeitVon = dataToSave.sollUhrzeitVon;
      this.beleg.sollUhrzeitBis = dataToSave.sollUhrzeitBis;

      this.beleg.istUhrzeitVon = this.beleg.istUhrzeitVon != null ? this.beleg.istUhrzeitVon : null;
      this.beleg.istUhrzeitBis = null;

    // this.beleg.pat = dataToSave.pat;

    if(handleStart) {
      this.beleg.istUhrzeitVon = new Date();
      this.beleg.currentAktive = true;
    }
    if(handleEnd) {
      this.beleg.istUhrzeitBis = new Date();
      this.beleg.currentAktive = false;
      this.beleg.finished = true;
    }


    this.belegung.save(this.beleg).then((data: any) => {
      console.dir(data);
      if(data != null && data.id != null) {
        this.beleg = data;
        let toast = this.toastCtrl.create({
          message: 'Speichern erfolgreich!',
          duration: 3000
        });
        toast.present().then(() => {
          this.navCtrl.pop();
        });
      } else {
        let toast = this.toastCtrl.create({
          message: 'Speichern gescheitert!',
          duration: 3000
        });
        toast.present();
      }
    });
  }

  handlePat() {
    if(this.beleg.currentAktive) {
      this.saveBeleg(false, true);
    } else {
      if(this.beleg && this.beleg.finished) {
        // ... reset
        this.beleg.istUhrzeitVon = null;
        this.beleg.istUhrzeitBis = null;
        this.beleg.finished = false;
        this.saveBeleg();
      } else {
        this.saveBeleg(true);
      }
    }

  }

  getCorrectHandleIcon() {
    if(this.beleg && this.beleg.currentAktive) {
      return "checkmark-circle-outline";
    } else {
      if(this.beleg && this.beleg.finished) {
        return "undo";
      } else {
        return "log-in";
      }
    }
  }

  searchPat() {
    /*
    let searchModal = this.modalCtrl.create('PatSuchePage', { modal: true });
    searchModal.onDidDismiss(data => {
      console.log(data);
    });
    searchModal.present();
    */

    new Promise((resolve, reject) => {
      this.navCtrl.push('PatSuchePage', { modal: resolve });
    }).then((pat: any) => {
      // now fill in pat field in form



      this.belegungForm.controls['pat'].setValue(pat);
    });


  }

}
