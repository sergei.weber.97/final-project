import { Component } from '@angular/core';
import {IonicPage, MenuController, NavController, NavParams, ToastController} from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BelegungEinsehenDetailPage} from "../belegung-einsehen-detail/belegung-einsehen-detail";
import {BelegungProvider} from "../../providers/belegung/belegung";

/**
 * Generated class for the BelegungEinsehenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-belegung-einsehen',
  templateUrl: 'belegung-einsehen.html',
})
export class BelegungEinsehenPage {

  public belegungForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, menu: MenuController, private formBuilder: FormBuilder, public toastCtrl: ToastController, public beleg: BelegungProvider) {
    menu.enable(false);

    this.belegungForm = this.formBuilder.group({
      token: ["", [Validators.required, Validators.pattern(".*")]]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BelegungEinsehenPage');
  }

  viewBeleg() {
    this.beleg.getByToken(this.belegungForm.value.token).then((data) => {
      console.log("tokenbeleg");
      console.dir(data);
      this.navCtrl.push('BelegungEinsehenDetailPage', {belegung: data});
    }).catch((err) => {
      let toast = this.toastCtrl.create({
        message: 'Abfrage gescheitert! Token falsch?',
        duration: 3000
      });
      toast.present();
    });

  }
}
