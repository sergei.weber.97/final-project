import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MitarbeiterProvider} from "../../providers/mitarbeiter/mitarbeiter";
import {RaumProvider} from "../../providers/raum/raum";

/**
 * Generated class for the OptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-options',
  templateUrl: 'options.html',
})
export class OptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public mitarbeiter: MitarbeiterProvider, public raum: RaumProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OptionsPage');
  }

  openMitarbeiterVerwaltung() {
    this.mitarbeiter.getMitarbeiter().then((lstmit) => {
      this.navCtrl.push('MitarbeiterVwPage', {mitarbeiterlst: lstmit});
    });
  }

  openRaumVerwaltung() {
    this.raum.getAllRaeume().then((lstRooms) => {
      this.navCtrl.push('RaumVwPage', {raumlst: lstRooms});
    });
  }

}
