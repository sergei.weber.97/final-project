import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RaumCreatePage } from './raum-create';

@NgModule({
  declarations: [
    RaumCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(RaumCreatePage),
  ],
})
export class RaumCreatePageModule {}
