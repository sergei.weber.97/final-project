import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as moment from 'moment';
import 'moment/locale/de';
import {RegistryProvider} from "../registry/registry";

/*
  Generated class for the PatientProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PatientProvider {

  apiUrl;

  constructor(public http: HttpClient, reg: RegistryProvider) {
    console.log('Hello PatientProvider Provider');
    this.apiUrl = reg.getApiUrl();
  }

  getById(id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/patienten/'+id).subscribe(data => {
        //...
        resolve(this.addOwnToString(data));
      }, err => {
        console.log(err);
      });
    });
  }

  private dateToString(input) {
    let date = moment(input);

    return date.format("DD.MM.YYYY");
  }

  public addOwnToString(patEntityObject) {
    if(patEntityObject == null)
      return null;
    // for readability, overload toString method ...
    var that = this;
    patEntityObject.toString = function() {
      return this.nachname + ", " + this.vorname + ", *" + that.dateToString(this.geburtsdatum);
    };

    return patEntityObject;
  }

  suche(filter) {
    return new Promise(resolve => {
      let bday: any = null;
      if(filter.bday != null && filter.bday != "")
        bday = new Date(filter.bday);

      this.http.get(this.apiUrl + '/svc/patienten/findPatientenByFilter?name='+filter.name+'&vorname='+filter.vorname+'&bday='+(bday != null ? bday.getTime() : "")).subscribe((data:any) => {
        //...

        for(var i = 0; i < data.length; i++) {
          data[i] = this.addOwnToString(data[i]);
        }

        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  delete(id) {
    return new Promise(resolve => {
      this.http.delete(this.apiUrl + '/patienten/'+id).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  save(dat) {
    return new Promise(resolve => {
      this.http.post(this.apiUrl + '/patienten', dat).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

}
