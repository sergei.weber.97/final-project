import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the RegistryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RegistryProvider {

  private apiUrl = 'http://im-vm-035:8080/hallinto/v1';
  // private apiUrl = 'http://localhost:8080/hallinto/v1';

  constructor(public http: HttpClient) {
    console.log('Hello RegistryProvider Provider');
  }


  getApiUrl() {
    return this.apiUrl;
  }

}
