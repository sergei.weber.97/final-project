import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {RegistryProvider} from "../registry/registry";

/*
  Generated class for the MitarbeiterProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MitarbeiterProvider {

  apiUrl;

  loggedOnUser = "";
  loggedOnToken = "";

  constructor(public http: HttpClient, reg: RegistryProvider) {
    console.log('Hello MitarbeiterProvider Provider');
    this.apiUrl = reg.getApiUrl();
  }

  getMitarbeiter() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/mitarbeiter').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getById(id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/mitarbeiter/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  isAuthenticated() {
    if(this.loggedOnUser == "") {
      return false;
    } else {
      return true;
    }
  }

  getAuthenticatedUsername() {
    if(this.isAuthenticated())
      return this.loggedOnUser;
    else
      return false;
  }

  private setAuthenticatedUsername(sUser) {
    this.loggedOnUser = sUser;
  }

  login(loginData: any) {
    console.dir(loginData);

    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl + '/mitarbeiterservice/login/'+loginData.username+'?password='+loginData.password).subscribe(data => {
        if(data) {
          this.setAuthenticatedUsername(loginData.username);
        }
        resolve(data);
      }, err => {
        console.log(err);
        reject(false);
      });
    });
  }


  delete(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.apiUrl + '/mitarbeiter/'+id).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  save(dat) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/mitarbeiter', dat).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

}
