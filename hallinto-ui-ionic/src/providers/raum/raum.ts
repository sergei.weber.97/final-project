import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {RegistryProvider} from "../registry/registry";

/*
  Generated class for the RaumProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RaumProvider {

  apiUrl;

  constructor(public http: HttpClient, reg: RegistryProvider) {
    console.log('Hello RaumProvider Provider');
    this.apiUrl = reg.getApiUrl();
  }


  getAllRaeume() {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/raum').subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  getById(id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl+'/raum/'+id).subscribe(data => {
        resolve(data);
      }, err => {
        console.log(err);
      });
    });
  }

  delete(id) {
    return new Promise((resolve, reject) => {
      this.http.delete(this.apiUrl + '/raum/'+id).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

  save(dat) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/raum', dat).subscribe(data => {
        //...
        resolve(data);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
  }

}
