import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { MitarbeiterProvider } from '../providers/mitarbeiter/mitarbeiter';

import { HttpClientModule } from '@angular/common/http';
import { PatientProvider } from '../providers/patient/patient';
import { RaumProvider } from '../providers/raum/raum';
import { BelegungProvider } from '../providers/belegung/belegung';
import { RegistryProvider } from '../providers/registry/registry';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    MitarbeiterProvider,
    PatientProvider,
    RaumProvider,
    BelegungProvider,
    RegistryProvider
  ]
})
export class AppModule {}
